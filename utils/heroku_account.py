import os, heroku

class HerokuAccount:

	__conn = None
	__app = None

	def __init__(self):
		try:
			heroku_api_key = os.environ['HEROKU_API_KEY']
			self.connect(heroku_api_key)
		except KeyError:
			print "Invalid Heroku API Key"
		except Exception as e:
			print "Error: " + str(e)

	def connect(self, heroku_api_key):
		self.__conn = heroku.from_key(heroku_api_key)
		self.__app = self.__conn.apps['simuladoresdecredito-worker']

	def count_instances(self):
		return len(list(self.__app.processes['worker']))

	def scale_one(self):
		count = self.count_instances()
		if count < 3:
			print "Scaling by 1"
			self.__app.processes['worker'].scale(count+1)

	def kill_one(self):
		count = self.count_instances()
		if count > 1:
			print "Killing 1 instance"
			#self.__app.processes[0].stop()
			self.__app.processes['worker'].scale(count-1)

from boto.sqs.connection import SQSConnection
import os

class Queue:
	
	__conn = None

	def __init__(self):
		try:
			aws_access_key_id = os.environ['AWS_ACCESS_KEY_ID']
			aws_secret_access_key = os.environ['AWS_SECRET_ACCESS_KEY']
			self.connect(aws_access_key_id, aws_secret_access_key)
		except KeyError:
			print "Invalid AWS keys"
		except Exception as e:
			print "Error: " + str(e)

	def connect(self, aws_access_key_id, aws_secret_access_key):
		try:
			self.__conn = SQSConnection(is_secure=False, validate_certs=False, debug=1, aws_access_key_id=aws_access_key_id , aws_secret_access_key=aws_secret_access_key)
		except:
			print "Incorrect key values. No connection established"

	def count(self):
		credits_queue = self.__conn.get_queue('Creditos_Queue')
		queue_count = credits_queue.count()
		return queue_count
